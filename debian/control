Source: geneagrapher
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Doug Torrance <dtorrance@debian.org>
Build-Depends: cmark-gfm <!nodoc>,
               debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3,
               python3-poetry-core,
               python3-pytest <!nocheck>,
               python3-websockets <!nocheck>
Standards-Version: 4.6.2
Homepage: https://github.com/davidalber/Geneagrapher
Vcs-Browser: https://salsa.debian.org/math-team/geneagrapher
Vcs-Git: https://salsa.debian.org/math-team/geneagrapher.git
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: geneagrapher
Architecture: all
Depends: python3-geneagrapher (= ${source:Version}),
         ${misc:Depends},
         ${python3:Depends}
Description: Create tree from Mathematics Genealogy Project (executable)
 The Mathematics Genealogy Grapher (Geneagrapher) is a software tool to gather
 the information for building math genealogy trees with data from the
 Mathematics Genealogy Project. The information extracted is stored in dot file
 format, which can then be passed to Graphviz to generate a graph.
 .
 This package contains the command-line tool.

Package: python3-geneagrapher
Section: python
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Recommends: graphviz
Breaks: geneagrapher (<< 1.0c2+git20220208-1)
Replaces: geneagrapher (<< 1.0c2+git20220208-1)
Description: Create tree from Mathematics Genealogy Project (module)
 The Mathematics Genealogy Grapher (Geneagrapher) is a software tool to gather
 the information for building math genealogy trees with data from the
 Mathematics Genealogy Project. The information extracted is stored in dot file
 format, which can then be passed to Graphviz to generate a graph.
 .
 This package contains the Python module.
