geneagrapher (2.0.0-1) unstable; urgency=medium

  * Upload to unstable now that bookworm has been released.

 -- Doug Torrance <dtorrance@debian.org>  Sun, 11 Jun 2023 09:10:23 -0400

geneagrapher (2.0.0-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Upload to experimental due to bookworm freeze.
  * debian/clean
    - Clean all html files, not just README.html (so essentially, also
      clean CHANGELOG.html).
  * debian/control
    - Drop db-util and libdb1-compat from Build-Depends; we no longer need
      to update the database files for the tests to pass.
    - Remove python3-bs4 and python3-lxml from Build-Depends; these
      dependencies have been moved from geneagrapher to geneagrapher-core.
    - Drop python3-setuptools from Build-Depends; replaced by
      pybuild-plugin-pyproject and python3-poetry-core.
    - Add python3-pytest and python3-websockets to Build-Depends; needed
      for tests.
    - Bump Standards-Version to 4.6.2.
    - Switch Testsuite to autopkgtest-pkg-pybuild.
    - Drop python3-pkg-resources from Depends; no longer used.
  * debian/docs
    - Install CHANGELOG.html instead of HACKING.txt and NEWS.txt, both of
      which were removed.
  * debian/patches
    - Remove patches that are no longer necessary.
      + remove-shelve-database-files.patch
      + skip-network-tests.patch
  * debian/patches/privacy-breach-generic.patch
    - New patch; use local images in README to fix privacy-breach-generic
      Lintian warning.
  * debian/rules
    - Stop setting variable for use by skip-network-tests.patch; it was
      removed.
    - Build CHANGELOG.html using cmark-gfm.
  * debian/tests/control
    - Remove unittest; now taken care of by pybuild.
    - Add ":a" flag to get Euler's ancestors for autopkgtest.
    - Use -q so progress bar isn't output to stderr.
  * debian/watch
    - Update; upstream is tagging releases again.

 -- Doug Torrance <dtorrance@debian.org>  Sat, 13 May 2023 06:32:37 -0400

geneagrapher (1.0c2+git20220519-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Bump Standards-Version to 4.6.1.
  * debian/gbp.conf
    - New file; set debian-branch to debian/latest; recommended by DEP-14.
  * debian/rules
    - Stop updating database files during build; updated files are now
      shipped upstream.

 -- Doug Torrance <dtorrance@debian.org>  Thu, 19 May 2022 22:08:23 -0400

geneagrapher (1.0c2+git20220208-2) unstable; urgency=medium

  * Source-only upload for transition to testing.
  * debian/control
    - Add dh-sequence-python3 to Build-Depends.
  * debian/rules
    - Drop "--with python3"; not necessary now that we have
      dh-sequence-python3 in Build-Depends.
    - Add new DB_FILES Makefile variable to simplify code.
  * debian/upstream/metadata
    - Add "---" for document start.

 -- Doug Torrance <dtorrance@debian.org>  Sat, 05 Mar 2022 21:33:38 -0500

geneagrapher (1.0c2+git20220208-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/clean
    - Clean up generated README.html
  * debian/control
    - Update Maintainer to Debian Math Team.
    - Update my email address (now a Debian Developer).
    - Add cmark-gfm to Build-Depends for generating html README.
    - Add db-util and libdb1-compat to Build-Depends; needed for updating
      database files for tests.
    - Update Vcs-* fields (science-team -> math-team).
    - Add Testsuite field for autodep8 test.
    - Split into two binary packages: geneagrapher for the command-line
      tool and python3-geneagrapher for the Python module.
  * debian/copyright
    - Update my copyright years and email address.
  * debian/docs
    - Install html version of README and accompanying images.
  * debian/*.install
    - Add files for dh_install to install the correct files in each binary
      package.
  * debian/patches/skip-network-tests.patch
    - Rewrite so that we only skip the network tests if the environment
      variable SKIP_NETWORK_TESTS is set to true.  This way, we can run
      them with autopkgtest.
  * debian/rules
    - Export SKIP_NETWORK_TESTS environment variable so we skip the tests
      requiring network access during build.
    - Generate html version of README.
    - Update database files from legacy format when running tests
  * debian/tests/control
    - Add test name for existing test.
    - Add unit tests.

 -- Doug Torrance <dtorrance@debian.org>  Wed, 02 Mar 2022 12:35:31 -0500

geneagrapher (1.0c2+git20200719-1) unstable; urgency=medium

  * New upstream release (git snapshot).
  * debian/control
    - Reformat using wrap-and-sort
    - Bump debhelper compatibility level to 13.
    - Bump Standards-Version to 4.6.0.
    - Drop Testsuite field; not using autodep8.
    - Add python3-pkg-resources to Depends; pkg_resources is imported by
      geneagrapher.
  * debian/patches
    - Refresh and remove patches applied upstream.
  * debian/patches/remove-shelve-database-files.patch
    - New patch; remove shelve database files regardless of extension.
  * debian/patches/skip-network-tests.patch
    - Add Forwarded field (not-needed).
  * debian/salsa-ci.yml
    - Add Salsa pipeline config file.
  * debian/test/control
    - New DEP-8 autopkgtest control file; includes a small test to check
      that we can fetch Euler's tree data.

 -- Doug Torrance <dtorrance@piedmont.edu>  Wed, 25 Aug 2021 00:49:56 -0400

geneagrapher (1.0c2+git20180919-2) unstable; urgency=medium

  * Team upload.
  * Port to Python 3 (Closes: #936596).
  * Use debhelper-compat (= 12).
  * Add Rules-Requires-Root: no.
  * Update Standards-Version to 4.5.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Sun, 09 Feb 2020 14:56:53 +1100

geneagrapher (1.0c2+git20180919-1) unstable; urgency=medium

  * New upstream release (git snapshot).
    - Port to Beautiful Soup 4 (Closes: #891107).
  * debian/clean
    - Remove distribute files, as distribute is no longer used.
    - Update path to egg files.
  * debian/compat
    - Bump debhelper compatibility level to 11.
  * debian/control
    - Add dh-python, python-bs4, and python-lxml to Build-Depends.
    - Bump versioned dependency on debhelper to >= 11.
    - Bump Standards-Version to 4.2.1.
    - Update Homepage.
    - Update Vcs-* after move to Salsa.
    - Add DEP-8 testsuite.
  * debian/copyright
    - Use https in Format.
    - Remove entries for the now-deleted file bootstrap.py and the
      corresponding ZPL-2.1 license.
  * debian/docs
    - README.rst was renamed to README.md.
  * debian/patches/dont_download_distribute.patch
    - Remove patch; distribute is no longer used during build.
  * debian/patches/include-test-data.patch
    - New patch; include test data files.
  * debian/patches/skip-network-tests.patch
    - New patch; skip all tests requiring network connection.
  * debian/rules
    - Use pybuild.
    - Remove get-orig-source target.
  * debian/upstream/metadata
    - Add DEP-12 upstream metadata file.
  * debian/watch
    - Bump to uscan v4.
    - Use git mode to generate version number.  In particular, we use
      1.0c2+git%cd, as upstream has not released a new version since
      1.0c2, but has been pushing new commits to git.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 20 Sep 2018 16:47:01 -0400

geneagrapher (1.0c2+git20120704-2) unstable; urgency=medium

  * debian/{control,copyright,ggrapher.1,patches}
    - Update my email address.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 03 Sep 2015 23:03:22 -0400

geneagrapher (1.0c2+git20120704-1) unstable; urgency=medium

  * Initial release (Closes: #779983).

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Sat, 07 Mar 2015 08:37:35 -0600
